Sample configuration files for:

SystemD: opcd.service
Upstart: opcd.conf
OpenRC:  opcd.openrc
         opcd.openrcconf
CentOS:  opcd.init
OS X:    org.opc.opcd.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
