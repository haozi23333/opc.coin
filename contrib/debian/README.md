
Debian
====================
This directory contains files used to package opcd/opc-qt
for Debian-based Linux systems. If you compile opcd/opc-qt yourself, there are some useful files here.

## opc: URI support ##


opc-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install opc-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your opc-qt binary to `/usr/bin`
and the `../../share/pixmaps/opc128.png` to `/usr/share/pixmaps`

opc-qt.protocol (KDE)

